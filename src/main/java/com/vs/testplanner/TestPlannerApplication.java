package com.vs.testplanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestPlannerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestPlannerApplication.class, args);
	}

}
